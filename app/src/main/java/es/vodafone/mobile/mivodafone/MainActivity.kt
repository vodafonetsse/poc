package es.vodafone.mobile.mivodafone

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebViewClient
import android.webkit.WebView
import android.widget.Button
import android.app.ProgressDialog
import android.widget.TextView
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    private var disposable: Disposable? = null

    private val wikiApiServe by lazy {
        GetDataService.create()
    }
    var progressDoalog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val getData  = findViewById(R.id.button4) as Button
        val loginrequest  = findViewById(R.id.button5) as Button
        val lblResult  = findViewById(R.id.lblResult) as TextView
       // TealiumHelper.init();

        getData.setOnClickListener({
            lblResult.text = "Please wait"
                beginSearch("karim")
        })

        loginrequest.setOnClickListener({
            lblResult.text = "Please wait"
                appSettings("")
        })

    }

    private fun beginSearch(searchString: String) {
        disposable = wikiApiServe.hitCountCheck("query", "json", "search", searchString)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> lblResult.text = "Request Done Successfully" },
                        { error -> Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show() }
                )
    }

    private fun appSettings(searchString: String) {
        disposable = wikiApiServe.systemSetting()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> lblResult.text = "Request Done Successfully" },
                        { error -> Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show() }
                )
    }
    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }

}

private operator fun CharSequence.plusAssign(message: String?) {

}

object Model {
    data class Result(val query: Query)
    data class Query(val searchinfo: SearchInfo)
    data class SearchInfo(val totalhits: Int)
    data class AppSettings(val forceupdate:String)

}