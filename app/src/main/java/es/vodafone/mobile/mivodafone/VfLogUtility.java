package es.vodafone.mobile.mivodafone;

import android.util.Log;

import java.lang.reflect.Field;

public class VfLogUtility {
    public static boolean enableLog;

    public static void infoLog(String tag, String message) {
        if (enableLog) {
            Log.i(tag, "" + tag + ":" + message);
        }
    }


    public static void errorLog(String tag, String message) {
        if (enableLog) {
            Log.e(tag, "" + tag + ":" + message);
        }

    }


    public static void warningLog(String tag, String message) {
        if (enableLog) {
            Log.w(tag, "" + tag + ":" + message);
        }

    }

    public static void verboseLog(String tag, String message) {
        if (enableLog) {
            Log.v(tag, "" + tag + ":" + message);
        }

    }


    public static void debugLog(String tag, String message) {
        if (enableLog) {
            Log.d(tag, "" + tag + ":" + message);
        }

    }



    public static void logNullFields(Object o) {
        for (Field field : o.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if (field.get(o) == null) {
//                    FabricUtil.crashlyticsLog(
//                            Log.INFO,
//                            "NULL_FIELD",
//                            "Null field: " + o.getClass().getSimpleName() + "[" + field.getName() + "]"
//                    );
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }



    public static class LogData {
        public final int logLevel;
        public final String tag;
        public final String message;

        public LogData(int logLevel, String tag, String message) {
            this.logLevel = logLevel;
            this.tag = tag;
            this.message = message;
        }
    }
}
