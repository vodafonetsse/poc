package es.vodafone.mobile.mivodafone;

import android.app.Application;

import com.tealium.library.Tealium;
import com.tealium.lifecycle.LifeCycle;

public class TealiumHelper {
    public static final String TEALIUM_INSTANCENAME = "vodafone";
    public static void init(Application application) {
        if(true) {
            Tealium.Config config = Tealium.Config.create(application, "vodafone", "es-mivoapp", "dev");

            // setup the lifecycle tracking instance
            boolean isAutoTracking = true;
            LifeCycle.setupInstance(TEALIUM_INSTANCENAME, config, isAutoTracking);

            // create a new Tealium instance with the specified instance name
            Tealium.createInstance(TEALIUM_INSTANCENAME, config);
            TealiumHelper.getInstance().addRemoteCommand(new SoastaRemoteCommand(application));
            // initialize our VFAnalytics framework
        }
    }
    public static Tealium getInstance() {
        return Tealium.getInstance(TEALIUM_INSTANCENAME);
    }
}
