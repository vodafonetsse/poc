package es.vodafone.mobile.mivodafone;

import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Decryptor {
    private static final String ALGO = "AES";
    private static String gk =""; //This value I send you in next mail
    private static final byte[] keyValue =
            new byte[]{'T', 'h', 'e', 'B', 'e', 's', 't', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};
    private static byte[] iv = new byte[]{ 0x07, 0x1f, 0x0a, 0x53, 0x21, 0x0e, 0x46, 0x12, 0x08, 0x3c ,0x02, 0x4b, 0x14, 0x0d, 0x34, 0x01};

    /**
     * Encrypt a string with AES algorithm.
     *
     * @param data is a string
     * @return the encrypted string
     */
    public static String encrypt(String data) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        return "";//new BASE64Encoder().encode(encVal);
    }

    /**
     * Decrypt a string with AES algorithm.
     *
     * @param encryptedData is a string
     * @return the decrypted string
     */
    public static String decrypt(String encryptedData) throws Exception {
        Cipher ecipher = null;

        AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
        SecretKey sk = new SecretKeySpec(gk.getBytes(),"AES");

        ecipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        ecipher.init(Cipher.DECRYPT_MODE, sk, paramSpec);

//        byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
//        byte[] decValue = ecipher.doFinal(decordedValue);
        return "";//new String(decValue);
    }

    /**
     * Generate a new encryption key.
     */
    private static Key generateKey() throws Exception {
        return new SecretKeySpec(keyValue, ALGO);
    }
}
//User will be: RICARDO.BARZANO@VODAFONE.COM
//Password will be: Prueba2468
//encrypted Value
//9HTohQ4IQs7kMQE5KOWO/nvt/xnaHEhUwK9X4QQkEfsGtGG04TdeYRiZdq+3+iLd3LMOoilTFjiA+IZZ/gSba1fmK2HGP5NdyVdBYCeziCxHS4nia+th+xOH0Wnl5GMhkVz4of5IW8ntpoQBvRBepel6Ivt4Ns82h+4QiA6PnvA=

//decy:
//logado;EMAIL;RICARDO.BARZANO@VODAFONE.COM;Prueba2468;8ead2960452926f7;1536852653040&_&da1a8b4a23fdb49cd5892c248dc0547c6efb33