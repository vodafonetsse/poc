package es.vodafone.mobile.mivodafone;

import android.app.Application;


public class POCApplication  extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            TealiumHelper.init(this);
        }
        catch (Exception ex){
            System.out.println("Exception:"+ex.getMessage());
        }
    }
}
