package es.vodafone.mobile.mivodafone

import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

interface GetDataService {
//    @get:GET("/photos")
//    val allPhotos: Call<List<RetroPhoto>>
//
    @GET("/es/v1/appSettings/settings")
    fun systemSetting(): Observable<Model.AppSettings>

    @GET("api.php")
    fun hitCountCheck(@Query("action") action: String,
                      @Query("format") format: String,
                      @Query("list") list: String,
                      @Query("srsearch") srsearch: String):
            Observable<Model.Result>

    companion object {
        fun create(): GetDataService {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://eu2-stagingref.api.vodafone.com/")
                    .build()

            return retrofit.create(GetDataService::class.java)
        }
    }

}
