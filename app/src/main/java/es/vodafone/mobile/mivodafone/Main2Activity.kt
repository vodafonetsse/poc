package es.vodafone.mobile.mivodafone

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.JsResult
import android.webkit.WebView
import android.webkit.WebChromeClient
import android.webkit.WebViewClient



class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        var tokenValidationWebview = findViewById<WebView>(R.id.token_validation_webview);
        tokenValidationWebview.getSettings().setJavaScriptEnabled(true)
        tokenValidationWebview.getSettings().setDomStorageEnabled(true)
        tokenValidationWebview.setWebChromeClient(object : WebChromeClient() {
            override fun onJsAlert(view: WebView, url: String, message: String, result: JsResult): Boolean {
                //Required functionality here
                return super.onJsAlert(view, url, message, result)
            }

        })
        tokenValidationWebview.loadUrl("file:///android_asset/index.html")
    }
}
